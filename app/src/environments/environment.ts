export const environment = {
  production: false,
  apiUrl: 'http://localhost:3000',
  essentialUrl: 'https://shorturl.at/cdlm2'
}
