export const environment = {
  production: true,
  apiUrl: 'http://localhost:3000',
  essentialUrl: 'https://shorturl.at/cdlm2'
}
