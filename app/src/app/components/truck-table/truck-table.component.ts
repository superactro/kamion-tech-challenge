import { Component, OnInit, ViewChild } from '@angular/core'
import { MatPaginator, PageEvent } from '@angular/material/paginator'
import { MatTableDataSource } from '@angular/material/table'
import { TruckHttpService } from '../../services/truck-http.service'
import { Params, Router, RoutesRecognized } from '@angular/router'
import { FormGroup, FormBuilder } from '@angular/forms'
import { debounceTime } from 'rxjs/operators'
import { LoadingService } from '../../services/loading.service'
import { ITruckItem } from './ITruckData'

@Component({
  selector: 'app-truck-table',
  templateUrl: './truck-table.component.html',
  styleUrls: ['./truck-table.component.scss']
})
export class TruckTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator
  public filterForm: FormGroup
  public pageSize = [10, 15, 20]
  public displayedColumns = [
    'truck_no',
    'make',
    'model',
    'year',
    'vin',
    'registration_no'
  ]
  public truckDataSource = new MatTableDataSource<ITruckItem>()

  constructor(
    private truckHttpService: TruckHttpService,
    private router: Router,
    private fb: FormBuilder,
    public loadingService: LoadingService
  ) { }

  ngOnInit(): void {
    this.filterForm = this.fb.group({
      truck_no: null,
      make: null,
      model: null,
      year: null,
      vin: null,
      registration_no: null
    })

    this.checkParams()
    this.onFilterChanges()
  }

  getTruckData(params: Params): void {
    this.truckHttpService.get(params).subscribe(({ count, rows }) => {
      this.loadingService.stop()
      this.paginator.length = count
      this.paginator.pageIndex = params.page
      this.truckDataSource.data = rows
    })
  }

  checkParams(): void {
    // activatedRoute.queryParams does not capture initial params so we use:
    this.router.events.subscribe((event) => {
      this.loadingService.start()
      if (!!event && event instanceof RoutesRecognized) {
        const { queryParams } = event.state.root
        if (
          queryParams.hasOwnProperty('page') &&
          queryParams.hasOwnProperty('limit')
        ) {
          return this.getTruckData(queryParams)
        }
        this.navigate({
          limit: this.pageSize[0],
          page: 0
        })
      }
    })
  }

  onFilterChanges(): void {
    this.filterForm.valueChanges
      .pipe(debounceTime(500))
      .subscribe((inputValues) => {
        this.paginator.pageIndex = 0
        this.navigate({
          ...inputValues,
          ...{ page: 0 }
        })
      })
  }

  public paginatorChange({ pageSize, pageIndex }: PageEvent): void {
    this.navigate({
      limit: pageSize,
      page: pageIndex
    })
  }

  public navigate(params: Params): void {
    this.router.navigate([], {
      queryParams: params,
      queryParamsHandling: 'merge',
      replaceUrl: true
    })
  }

  reset(): void {
    this.filterForm.reset()
  }
}
