export interface ITruckData {
  rows: Array<ITruckItem>
  count: number
}

export interface ITruckItem {
  truck_no: string
  make: string
  model: string
  year: string
  vin: string
  registration_no: string
}
