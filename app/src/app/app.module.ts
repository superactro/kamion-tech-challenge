import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { AppComponent } from './app.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MaterialModule } from './material/material.module'
import { TruckTableComponent } from './components/truck-table/truck-table.component'
import { TruckHttpService } from './services/truck-http.service'
import { HttpClient } from '@angular/common/http'
import { HttpClientModule } from '@angular/common/http'
import { RouterModule } from '@angular/router'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { LoadingService } from './services/loading.service'
import { LoadingComponent } from './components/loading/loading.component'

@NgModule({
  declarations: [AppComponent, TruckTableComponent, LoadingComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot([])
  ],
  providers: [HttpClient, TruckHttpService, LoadingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
