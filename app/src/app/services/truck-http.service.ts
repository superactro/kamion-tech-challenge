import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { environment } from 'src/environments/environment'
import { HttpClient } from '@angular/common/http'
import { Params } from '@angular/router'

@Injectable()
export class TruckHttpService {
  constructor(public httpClient: HttpClient) { }
  private apiUrl = environment.apiUrl

  public get(params?: Params): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/trucks`, { params })
  }
}
