import { Injectable } from '@angular/core'

@Injectable()
export class LoadingService {
  private loadingFlag = false

  get loading(): boolean {
    return this.loadingFlag
  }

  start(): void {
    this.loadingFlag = true
  }

  stop(): void {
    this.loadingFlag = false
  }
}
