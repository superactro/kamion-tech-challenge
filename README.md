# Development Environment

## Start project

Run `docker-compose up -d` for a dev environment.

## Preview

The app is on port 4200 and the server is on port 3000

Run `docker-compose logs -f` for logs.

## Database connection details

To change the database config edit PGUSER, PGHOST, PGPASSWORD, PGDATABASE, PGPORT in docker-compose.yaml

## Lint

For app run `cd app && npm run-script lint`
For server run `cd server && npm run-script lint`

## Tests

Tests not included in project scope
