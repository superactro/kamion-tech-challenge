import config from '../config/db.config'
import { Sequelize, DataTypes } from 'sequelize'

const sequelize = new Sequelize(config.db)

const Truck = sequelize.define(
  'truck',
  {
    truck_no: {
      type: DataTypes.TEXT,
    },
    make: {
      type: DataTypes.TEXT,
    },
    model: {
      type: DataTypes.TEXT,
    },
    year: {
      type: DataTypes.BIGINT,
    },
    vin: {
      type: DataTypes.TEXT,
    },
    registration_no: {
      type: DataTypes.TEXT,
    },
  },
  {
    timestamps: false,
  }
)

export default Truck
