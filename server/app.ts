import express, { Application, RequestHandler } from 'express'
import IControllerBase from './interfaces/IControllerBase.interface'

class App {
  public app: Application
  public port: number

  constructor(appInit: {
    port: number
    middleWares: RequestHandler[]
    controllers: IControllerBase[]
  }) {
    this.app = express()
    this.port = appInit.port

    this.middlewares(appInit.middleWares)
    this.routes(appInit.controllers)
  }

  private middlewares(middleWares: {
    forEach: (arg0: (middleWare: any) => void) => void
  }) {
    middleWares.forEach((middleWare) => {
      this.app.use(middleWare)
    })
  }

  private routes(controllers: {
    forEach: (arg0: (controller: any) => void) => void
  }) {
    controllers.forEach((controller) => {
      this.app.use('/', controller.router)
    })
  }

  public listen(): void {
    this.app.listen(this.port)
  }
}

export default App
