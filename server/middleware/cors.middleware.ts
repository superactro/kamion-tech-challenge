import { RequestHandler, Request, Response, NextFunction } from 'express'

const corsMiddleware: RequestHandler = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.set({
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET',
  })
  next()
}

export default corsMiddleware
