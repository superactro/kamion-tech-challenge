import App from './app'

import TruckController from './controllers/truck.controller'
import corsMiddleware from './middleware/cors.middleware'

const app = new App({
  port: 3000,
  controllers: [new TruckController()],
  middleWares: [corsMiddleware],
})

app.listen()
