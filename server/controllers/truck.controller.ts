import express, { Request, Response, NextFunction } from 'express'
import IControllerBase from '../interfaces/IControllerBase.interface'
import Truck from '../models/truck.model'
import { Op, Sequelize } from 'sequelize'

class HomeController implements IControllerBase {
  public router = express.Router()

  constructor() {
    this.initRoutes()
  }

  public initRoutes(): void {
    this.router.get('/trucks', this.index)
  }

  index = async (
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    if (!req.query.limit || !req.query.page) {
      next(new Error('Required query params missing'))
    }

    const where: any = {}
    for (const param in req.query) {
      if (param !== 'limit' && param !== 'page' && param !== 'year') {
        where[param] = {
          [Op.iLike]: `%${req.query[param]}%`,
        }
      } else if (param === 'year') {
        where[param] = {
          [Op.or]: [
            Sequelize.where(
              Sequelize.cast(Sequelize.col('truck.year'), 'varchar'),
              {
                [Op.iLike]: `%${req.query[param]}%`,
              }
            ),
          ],
        }
      }
    }

    const count = await Truck.count({ where })
    const trucks = await Truck.findAll({
      where,
      limit: Number(req.query.limit),
      offset: Number(req.query.page),
    })
    res.json({
      rows: trucks,
      count: count,
    })
  }
}

export default HomeController
